package com.calculator.test;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.apache.log4j.Logger;

public class Main {
	
	private static final String ADD = "add";
	private static final String SUB = "sub";
	private static final String MULT = "mult";
	private static final String DIV = "div";
	private static final String LET = "let";
	private static final String illegalArgMsg = "Input is not correct";

	final static Logger logger = Logger.getLogger(Main.class);
	
	private Map<String, LinkedList<Integer>> calcMap;
	
	/*
	 * constructor for the calculator
	 */
	public Main() {
		this.calcMap = new HashMap<String, LinkedList<Integer>>();
	}
	
	/*
	 * checking the matching parenthesis
	 *
	 */
	private boolean checkMatchingParanthesis(final String exp) {
		try {
			int counter = 0;
			for (int i = 0; i < exp.length(); i++) {
			
				if (exp.charAt(i) == '(') 
					counter++;
			
				if (exp.charAt(i) == ')') {
					if(counter == 0) 
						throw new IllegalArgumentException(illegalArgMsg);
					counter--;
				}
			
			}
		
			return counter == 0;
		} catch (Exception e){
			logger.error(e);
		}
		return false;
	}
	
	
	/*
	 * 
	 *Method for checking prefixes and parenthesis
	 */
	
	private static int nextDelim(final String exp, final int pre, final Character del) {
		
		int i = pre;
		try {
			int counter = 0;
			for (; i < exp.length(); i++) {
			
				if (counter == 0 && exp.charAt(i) == del)
					return i;
			
				if (exp.charAt(i) == '(') 
					counter++;
			
				if (exp.charAt(i) == ')') {
					if(counter == 0) 
						throw new IllegalArgumentException(illegalArgMsg);
					counter--;
				}
			
			}
		
			if(counter > 0) 
				throw new IllegalArgumentException(illegalArgMsg);
			
			} catch (Exception e){
				logger.error(e);
			}
			return i;
	}
	
	/*
	 * Checking the starting parenthesis
	 */
	private static void checkStartParenthesis(final String exp,final int pre) {
		try {
			if (!exp.startsWith("(", pre)) {
				throw new IllegalArgumentException(illegalArgMsg);
			}
		} catch (Exception e){
			logger.error(e);
			return;
		}
	}
	
	/*
	 * Checking syntax for the expressions add/sub/mul/div
	 */
	private void simpleExpSyntaxCheck(final String exp,final String op) {
		checkStartParenthesis(exp, op.length());
		
		int comma = nextDelim(exp, op.length() + 1, ',');
		String exp1 = exp.substring(op.length() + 1, comma);
		checkSyntax(exp1);
		
		int end = nextDelim(exp, comma + 1, ')');
		assert (end == exp.length() - 1);
		String exp2 = exp.substring(comma + 1, end);
		checkSyntax(exp2);
	}
	
	/*
	 * Checking syntax for let expression
	 */
	private void checkLetExpSyntax(final String exp,final String op) {
		checkStartParenthesis(exp, op.length());
		
		int comma = nextDelim(exp, op.length() + 1, ',');
		String var = exp.substring(op.length() + 1, comma);
		if(logger.isDebugEnabled()){
			logger.debug("let label = " + var);
		}
		checkSyntax(var);
		
		
		int secComma = nextDelim(exp, comma + 1, ',');
		String nameExpValue = exp.substring(comma + 1, secComma);
		if(logger.isDebugEnabled()){
			logger.debug("let valueExprName = " + nameExpValue);
		}
		checkSyntax(nameExpValue);
		
		
		int end = nextDelim(exp, secComma + 1, ')');
		String name = exp.substring(secComma + 1, end);
		if(logger.isDebugEnabled()){
			logger.debug("let exprName = " + name);
		}
		checkSyntax(name);
		
		
	}
	
	/*
	 * Checking for numeric expression
	 */
	private static boolean isNum(final String exp) {
			String eval = exp;
			if (exp.startsWith("-")) {
				eval = exp.substring(1,exp.length());
			}
	
		 for (Character a: eval.toCharArray()) {
			 	if (!Character.isDigit(a)) {
			 		return false;
			 	}
		 }
			
		return true;
	}
	
	/*
	 * Checking syntax for expression
	 */
	private void checkSyntax(final String exp) {
		
		try{
		 	if(exp.matches("[a-zA-z]+")) {
		 		
		 	} else if(isNum(exp)) {
		 		
		 	} else if (exp.startsWith(ADD)) {
		 		simpleExpSyntaxCheck(exp, ADD);
				
		 	} else if (exp.startsWith(SUB)) {
				simpleExpSyntaxCheck(exp, SUB);
				
		 	} else if (exp.startsWith(MULT)) {
				simpleExpSyntaxCheck(exp, MULT);
				
		 	} else if (exp.startsWith(DIV)) {
				simpleExpSyntaxCheck(exp, DIV);
				
		 	} else if (exp.startsWith(LET)) {
				simpleExpSyntaxCheck(exp, LET);
				
			} else {
				throw new IllegalArgumentException("Please provide a valid operation");
			}
			
			if (!checkMatchingParanthesis(exp)) 
				throw new IllegalArgumentException("Unmatched Parenthesis");
			
		} catch (Exception e){
			logger.error(e);
			return;
		}
			
	
			
	}
	
	
	/*
	 * Method for getting two expressions
	 */
	private String[] gettingTwoExp(final String exp,final String op) {
		
		String[] expArr = new String[2]; 
		
		int comma = nextDelim(exp, op.length() + 1, ',');
		String exp1 = exp.substring(op.length() + 1, comma);
		expArr[0] = exp1;
		
		int end = nextDelim(exp, comma + 1, ')');
		String exp2 = exp.substring(comma + 1, end);
		expArr[1] = exp2;
		
		return expArr;
	}
	
	/*
	 * Getter method with let expressions
	 */
	private String[] getWithLetExp(final String exp, final String op) {
		String[] expArr = new String[3]; 
		
		int comma = nextDelim(exp, op.length() + 1, ',');
		String lbl = exp.substring(op.length() + 1, comma);
		expArr[0] = lbl;
		if(logger.isDebugEnabled()){
			logger.debug("let lbl = " + lbl);
		}
		
		int secComma = nextDelim(exp, comma + 1, ',');
		String exp1 = exp.substring(comma + 1, secComma);
		expArr[1] = exp1;
		
		if(logger.isDebugEnabled()){
			logger.debug("let exp1 = " + exp1);
		}
		
		int end = nextDelim(exp, secComma + 1, ')');
		String exp2 = exp.substring(secComma + 1, end);
		expArr[2] = exp2;
		if(logger.isDebugEnabled()){
			logger.debug("let exp2 = " + exp2);
		}
		
		return expArr;
	
	}
	
	/*
	 * Evaluating the expression
	 */
	private int evalExpression(final String exp) {
	
	try {
		if(exp.matches("[a-zA-z]+")) {
			if (calcMap.containsKey(exp)) {
				return calcMap.get(exp).peek();
			} else {
				throw new IllegalArgumentException("Variable is not found in let");
				}
	 		
	 	} else if(isNum(exp)) {
	 		if(logger.isDebugEnabled()){
				logger.debug(Integer.parseInt(exp));
			}
			return Integer.parseInt(exp);
			
		} else if (exp.startsWith(ADD)) {
			
			String[] expr = gettingTwoExp(exp, ADD);
			if(logger.isDebugEnabled()){
				logger.debug("ADD Expression: Expr1 = " + expr[0] + ", Expr2 = " + expr[1]);
			}
			return evalExpression(expr[0]) + evalExpression(expr[1]);
			 
		} else if (exp.startsWith(SUB)) {
			
			String[] expr = gettingTwoExp(exp, SUB);
			if(logger.isDebugEnabled()){
				logger.debug("SUB Expression: Expr0 = " + expr[0] + ", Expr1 = " + expr[1]);
			}
			return evalExpression(expr[0]) - evalExpression(expr[1]);
		
		} else if(exp.startsWith(MULT)) {
			
			String[] expr = gettingTwoExp(exp, MULT);
			if(logger.isDebugEnabled()){
				logger.debug("MULT Expression: Expr0 = " + expr[0] + ", Expr1 = " + expr[1]);
			}
			return evalExpression(expr[0]) * evalExpression(expr[1]);
		
		} else if(exp.startsWith(DIV)) {
			
			String[] expr = gettingTwoExp(exp, DIV);
			if(logger.isDebugEnabled()){
				logger.debug("DIV Expression: Expr0 = " + expr[0] + ", Expr1 = " + expr[1]);
			}
			return evalExpression(expr[0]) / evalExpression(expr[1]);
		
		} else if(exp.startsWith(LET)) {
			String[] expr = getWithLetExp(exp, LET);
			String lbl = expr[0];
			String exp1 = expr[1];
			String exp2 = expr[2];
			if(logger.isDebugEnabled()){
				logger.debug("Let Expression: Label = " + lbl + ", Expr1 = " + exp1 + ", Expr2 = " + exp2);
			}
			int expVal1 = evalExpression(exp1);
			LinkedList<Integer> curr;
			if (!calcMap.containsKey(lbl)) {
				curr = new LinkedList<Integer>();
				calcMap.put(lbl, curr);
			}
			calcMap.get(lbl).push(expVal1);
			
			
			int expVal2 = evalExpression(exp2);
			
			LinkedList<Integer> prev = calcMap.get(lbl);
			prev.pop();
			if (prev.isEmpty()) {
				calcMap.remove(lbl);
			}
			
			return expVal2;
		
		}else {
			
		} 
		
	} catch (Exception e){
		logger.error(e);
	}
		
		return 0;
	}
	
	
	/*
	 * Calling main method
	 */
	public static void main(String[] args) {
		try {
			if (args.length != 1 ) {
				throw new IllegalArgumentException(illegalArgMsg);
			}
		} catch (Exception e){
			logger.error(e);
			return;
		}
		Main myCal = new Main();
		myCal.checkSyntax(args[0]);
		System.out.println(myCal.evalExpression(args[0]));
		
	}
	
}